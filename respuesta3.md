### 3 Comprensión ###

Con el siguiente código en python:


    # -*- coding: utf-8 -*-
    def z(c):
        if c[-1] == '?':
            return 3
        else:
            return 2
    
    def y(b):
        r = [l.isupper() for l in b[:-1] if l.isalpha()]
    
        if r and reduce(lambda a,x : a and x, r): # !
            return 4
        else:
            return z(b)
    
    
    def x(a):
        a = ''.join(a.strip().split(' '))
        return y(a) if a else 1


Llamadas:

* x('')
* x('1 2 3 4')
* x('1 2 3 4 A?')
* x('hola?')
* x('HOLA')

Explique según usted:

* ¿Qué hace este código?

    x('')  = como va vacio retorna 1 

    x('1 2 3 4') =  como no tiene valores numericos y el ultimo valor del string es un numero, asi que en la funcion z retorna  2 

    x('1 2 3 4 A?') = como tiene valor alfabeticos y mayuscula retorna 4

    x('hola?') = Tiene valores alfabeticos pero no mayusculas asi que en z retorna 3 porque tiene signo de pregunta en el último valor del string

    x('HOLA') = Como tiene valores alfabeticos y son mayusculas retorna 4



* ¿En la línea marcada con un # !: ¿Qué problema se puede presentar? ¿como se soluciona?

    cuando lo descompuse para entender que hacía reduce y lambda se me caía cuando estaba vacio, asi que le tuve que poner un try except para evitar que se cayera el código. No tengo más información de los requerimientos para mejorar el código. 

* ¿Qué mejora haría usted en la implementación del código?

    Me costó mucho entender como funcionaba el código así que no se me ocurre una mejora en estos momentos. 
    Tendría que evaluarlo con un poco mas de tiempo. 

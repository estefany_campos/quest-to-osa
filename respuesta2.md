### 2 Desarrollo de API ###
Si alguna vez has utilizado la API de Facebook, sabrás lo común que "deprecaban" su API. Según usted:

* Explique la razón por la cuál esta no es una buena práctica.

    Porque al final es un problema para el programador que está utilizando el código y sin previo aviso deja de funcionar y tiene que volver a ver la documentación e implementar nuevamente la nueva versión de la api.

* ¿Por qué se dice que las interfaces deben ser "tontas"?

    Por que les preguntan y responden a los requerimientos solamente, es lo que se me ocurre.

* Como aplicaria usted los conceptos de Encapsulamiento, Flexibilidad, Seguridad y Versionamiento en el desarrollo de una Interfaz?

    No soy muy buena respondiendo estas cosas pero esto es lo que aplicaría. 

    Encapsulamiento se definen los terminales y cada terminal usa los metodos http para resolver las tareas de la api.
    Flexibilidad sería haciendo consulta con varios parametros para que no devuelva una cantidad enorme de data. Seguridad con codigo encriptado.
    Versionamiento lo manejaría con git manejando las versiones estables y subiendo esas, así si ocurre un problema se puede volver a la versión anterior. 
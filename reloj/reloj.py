#!/usr/bin/python
# -*- coding: UTF-8 -*-

import unittest

class Reloj(object):
    """docstring for Reloj"""
    def __init__(self, hour, min):
        super(Reloj, self).__init__()
        self.hour = hour
        self.min = min

    def __eq__(self, other):

        return self.calc()

    def suma(self, num):
        self.min= self.min + num

        return self.calc()

    def calc(self):

        hour_min = self.min / 60
        mod_min = self.min % 60

        res_hour = (hour_min + self.hour) % 24

        if res_hour < 10:
            res_hour = '0'+str(res_hour)

        if mod_min < 10:
            mod_min = '0'+str(mod_min)

        return str(res_hour) +':'+ str(mod_min)


class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(Reloj(8, 0), '08:00')
        self.assertEqual(Reloj(72,8640), '00:00')
        self.assertEqual(Reloj(-1,15), '23:15')
        self.assertEqual(Reloj(-25,-160), '20:20')
        self.assertEqual(Reloj(10,3).suma(-70), '08:53')

if __name__ == "__main__":
    unittest.main()